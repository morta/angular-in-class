import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule,Routes} from '@angular/router';
import {AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PostsComponent } from './posts/posts.component';
import { UserFormComponent } from './user-form/user-form.component';
import { ProductsComponent } from './products/products.component';
import { ProductsService } from './products/products.service';
import { ProductComponent } from './product/product.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoicesComponent } from './invoices/invoices.component';

export const firebaseConfig = {
    apiKey: "AIzaSyBkrsYPBqm6U06SzaAzkiJ-1ESr_IE2aj0",
    authDomain: "first-project-b61a1.firebaseapp.com",
    databaseURL: "https://first-project-b61a1.firebaseio.com",
    storageBucket: "first-project-b61a1.appspot.com",
    messagingSenderId: "211239046919"
}

const appRoutes: Routes = [
//{path:'users', component: UsersComponent},
//{path:'posts', component: PostsComponent},
//{path:'products', component: ProductsComponent},
{path:'invoiceform', component: InvoiceFormComponent},
{path:'invoices', component: InvoicesComponent},
{path:'', component:InvoiceFormComponent},
{path:'**', component:PageNotFoundComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    PostsComponent,
    UserFormComponent,
    ProductsComponent,
    ProductComponent,
    InvoiceFormComponent,
    InvoicesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService, ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
