import { Component, OnInit } from '@angular/core';
import { ProductsService } from './products.service';

@Component({
  selector: 'jce-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products;
  constructor(private _productsService:ProductsService) { }

  deleteProduct(product){
    this._productsService.deleteProduct(product);
  }

  ngOnInit() {
    this._productsService.getProducts().subscribe(productsData => 
    {this.products = productsData;});
  }

}
