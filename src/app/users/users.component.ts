import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
    styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }  
  `]
})
export class UsersComponent implements OnInit {
isLoading:Boolean = true;
users;
currentUser;

select(user){
  this.currentUser = user; 
}

  constructor(private _usersService:UsersService) { }
  addUser(user){
    //this.users.push(user)
    this._usersService.addUser(user);
  }
 
    updateUser(user){
      this._usersService.updateUser(user);
  }


  deleteUser(user){
    //this.users.splice(
     // this.users.indexOf(user),1)
     this._usersService.deleteUser(user);
  }

  ngOnInit() {
    //this._usersService.getUsers().subscribe(usersData => 
    this._usersService.getUsersFromApi().subscribe(usersData => 
    {this.users = usersData.result;
      this.isLoading = false;
    console.log(this.users)});
}
}
